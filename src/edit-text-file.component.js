/*global angular*/
'use strict';

//noinspection JSUnresolvedFunction
angular.module('bitcraft-edit-text-file').
    component('editTextFile', {
        templateUrl: 'js/edit-text-file/edit-text-file.template.html',
        controller: [
            '$scope', 'RestHttp', 'Notification',
            function ($scope, RestHttp, Notification) {
                var self = this;

                this.saveFile = function () {
                    RestHttp.restPost('saveTextFileContent', {fileName: $scope.configFileName, content: $scope.aceSession.getDocument().getValue()}).then(function () {
                        Notification.success({
                            message: 'File saved successfully'
                        });
                    }, function (error) {
                        Notification.error({
                            message: 'Failed to save the config file (' + error.statusText + ')'
                        });
                    });
                };

                this.aceLoaded = function (e) {
                    $scope.aceSession = e.getSession();
                };

                this.$onInit = function () {
                    var info = $scope.$parent.$parent.db.data.info;
                    var i;

                    $scope.configFileName = null;
                    for (i = 0; i < info.length; i++) {
                        if (info[i].accessor === 'configuration') {
                            $scope.configFileName = 'c_' + info[i].value + '.conf';
                        }
                    }

                    if ($scope.configFileName !== null) {
                        RestHttp.restPost('getTextFileContent', {fileName: $scope.configFileName}).then(function (response) {
                            $scope.aceModel = response.data;
                        }, function (error) {
                            Notification.error({
                                message: 'Failed to retrieve the config file (' + error.statusText + ')'
                            });
                        });
                    }

                    // The ui-ace option
                    $scope.aceOption = {
                        mode: 'text',
                        onLoad: self.aceLoaded
                    };
                };
            }
        ]
    });
