'use strict';

// native
var fs = require('fs');
var sanitize = require('sanitize-filename');

// 3rd party
var log4js = require('log4js');
var mongo = require('expoj-mongo');
var returnCodes = require('laposte').returnCodes;

var exec = require('child_process').exec;

/** @type string */
var pathToFiles = './load-tests-launcher';

function getTextFileContent(request, callback) {
    if (!request) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }
    var data = request.data;
    if (!data.fileName) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }

    fs.readFile(pathToFiles + '/' + sanitize(data.fileName), 'utf8', function (err, data) {
        if (err) {
            callback(returnCodes.NOT_FOUND, err);
            return;
        }

        callback(returnCodes.OK, { data: data });
    });
}

function saveTextFileContent(request, callback) {
    if (!request) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }
    var data = request.data;
    if (!data.fileName || !data.content) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }

    //noinspection JSLint,JSDeprecatedSymbols
    fs.writeFile(pathToFiles + '/' + sanitize(data.fileName), data.content, function (err) {
        if (err) {
            callback(returnCodes.BAD_REQUEST, err);
            return;
        }

        callback(returnCodes.OK);
    });
}

module.exports = {
    getTextFileContent: {
        method: 'POST',
        callback: getTextFileContent,
        anonymous: true,
        contentType: 'application/json'
    },
    saveTextFileContent: {
        method: 'POST',
        callback: saveTextFileContent,
        anonymous: true,
        contentType: 'application/json'
    }
};
