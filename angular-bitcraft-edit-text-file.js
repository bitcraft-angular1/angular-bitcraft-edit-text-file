(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/*global angular*/
'use strict';

var template = '' +
'<style>' +
'    .ace_editor  {' +
'        height : 300px;' +
'        margin-bottom: 10px;' +
'        margin-top: 10px;' +
'    }' +
'</style>' +
'<hr>' +
'<div ng-if="configFileName !== null">' +
'    <button class="btn btn-default btn-block" ng-click="$ctrl.saveFile()">Save {{configFileName}}</button>' +
'    <div ui-ace="aceOption" ng-model="aceModel"></div>' +
'</div>' +
'';


//noinspection JSUnresolvedFunction
angular.module('bitcraft-edit-text-file').
    component('editTextFile', {
        template: template,
        controller: [
            '$scope', 'RestHttp', 'Notification',
            function ($scope, RestHttp, Notification) {
                var self = this;

                this.saveFile = function () {
                    RestHttp.restPost('saveTextFileContent', {fileName: $scope.configFileName, content: $scope.aceSession.getDocument().getValue()}).then(function () {
                        Notification.success({
                            message: 'File saved successfully'
                        });
                    }, function (error) {
                        Notification.error({
                            message: 'Failed to save the config file (' + error.statusText + ')'
                        });
                    });
                };

                this.aceLoaded = function (e) {
                    $scope.aceSession = e.getSession();
                };

                this.$onInit = function () {
                    var info = $scope.$parent.$parent.db.data.info;
                    var i;

                    $scope.configFileName = null;
                    for (i = 0; i < info.length; i++) {
                        if (info[i].accessor === 'configuration') {
                            $scope.configFileName = 'c_' + info[i].value + '.conf';
                        }
                    }

                    if ($scope.configFileName !== null) {
                        RestHttp.restPost('getTextFileContent', {fileName: $scope.configFileName}).then(function (response) {
                            $scope.aceModel = response.data;
                        }, function (error) {
                            Notification.error({
                                message: 'Failed to retrieve the config file (' + error.statusText + ')'
                            });
                        });
                    }

                    // The ui-ace option
                    $scope.aceOption = {
                        mode: 'text',
                        onLoad: self.aceLoaded
                    };
                };
            }
        ]
    });

},{}],2:[function(require,module,exports){
/*global angular*/
'use strict';

angular.module('bitcraft-edit-text-file', ['ui.ace']);

},{}],3:[function(require,module,exports){
'use strict';
require('./edit-text-file.module.js');
require('./edit-text-file.component.js');

},{"./edit-text-file.component.js":1,"./edit-text-file.module.js":2}]},{},[3]);
